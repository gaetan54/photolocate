<?php

session_start();

require_once 'vendor/autoload.php';

use photolocate\controller\controller;

$app = new \Slim\Slim();

/************************************** Middleware ******************************************/
$app->add(new \Slim\Middleware\CsrfGuard());

/************************************* Start Eloquents **************************************/
$eloquent = new photolocate\model\ConnectionFactory();
$eloquent->start();

/********** Page d'accueil : lance le jeu (root angular) ******/
$app->get('/', function () {
    $ctrl = new controller();
    $ctrl->afficheAccueil();
})->name('root');

/********* page scores : affiche le tableau de score ***********/
$app->get('/scores', function (){
    $ctrl = new controller();
    $ctrl->afficheScores();
})->name('hallOfFame');

/********** Partie administration : ajout de lieu ***************/
$app->get('/admin', function () {
    $ctrl = new controller();
    $ctrl->espaceAdmin();
})->name('espaceAdmin');

/********* Retour du formulaire d'ajout d'un lieu ***************/
$app->post('/ajoutPhoto', function () use ($app){
    $ctrl = new controller();
    $ctrl->ajoutPhotoAdmin($app);

})->name('ajoutPhotoAdmin');


/******************************** API ****************************/

/************************** Création partie **********************/
$app->post('/play/games', function (){
    $ctrl = new controller();
    $ctrl->creationPartie();
})->name('creaPartie');

/************************ MAJ partie *****************************/
$app->put('/play/games/:id', function ($id) use($app){
    $ctrl = new controller();
    $ctrl->majPartie($id, $app);
})->name('majPartie');


/******* API : Recuperer 10 photos avec coordonnées GPS ***********/
$app->get('/play/games/:id/photos/', function ($id) use($app){
    $ctrl = new controller();
    $ctrl->recupPhotos($id, $app);
})->name('photos');

/***************API : Récupérer description partie******************/
$app->get('/play/games/:id', function($id) use($app) {
    $ctrl = new controller();
    $ctrl->apiDescription($id, $app);
})->name('descriptionPartie');

/***************API : Récupérer les meilleurs scores****************/
$app->get('/play/scores', function() use($app) {
    $ctrl = new controller();
    $ctrl->apiScore($app);
})->name('HallOfFame');

/************************** Run slim *******************************/
$app->run();