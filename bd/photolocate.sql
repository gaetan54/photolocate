-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 09 Février 2015 à 17:05
-- Version du serveur: 5.5.40-0ubuntu0.14.04.1
-- Version de PHP: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `photolocate`
--

-- --------------------------------------------------------

--
-- Structure de la table `parties`
--

CREATE TABLE IF NOT EXISTS `parties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(45) NOT NULL,
  `score` int(11) DEFAULT NULL,
  `token` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(45) NOT NULL,
  `url` varchar(45) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `photos`
--

INSERT INTO `photos` (`id`, `titre`, `url`, `latitude`, `longitude`) VALUES
(1, 'Villa Majorelle', 'villa-majorelle.jpg', 48.6856, 6.16393),
(2, 'Tour Thiers', 'tourThiers.jpg', 48.6906, 6.17488),
(3, 'Basilique Sainte Epvre', 'saint_epvre.jpg', 48.6959, 6.17989),
(4, 'Porte Sainte Catherine', 'porteSainteCath.jpg', 48.6956, 6.18993),
(5, 'Place Stanislas', 'place-stanislas.jpg', 48.6939, 6.18303),
(6, 'Parc de la Pépinière', 'pepiniere.jpg', 48.6881, 6.17344),
(7, 'Stade Marcel Picot', 'marcelPicot.jpg', 48.6954, 6.21039),
(8, 'Faculté d''architecture', 'facArchi.jpg', 48.696, 6.19282),
(9, 'Excelsior', 'excelsior-nancy.jpg', 48.6905, 6.17591),
(10, 'Cathedrale', 'cathedrale.jpg', 48.6956, 6.18993);

-- --------------------------------------------------------

--
-- Structure de la table `photo_partie`
--

CREATE TABLE IF NOT EXISTS `photo_partie` (
  `photo_id` int(11) NOT NULL,
  `partie_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
