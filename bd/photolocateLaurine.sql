-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 11 Février 2015 à 23:54
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `lpsandbox`
--

-- --------------------------------------------------------

--
-- Structure de la table `channels`
--

CREATE TABLE IF NOT EXISTS `channels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_channels_user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `message` longtext,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_posts_user_id` (`user_id`),
  KEY `idx_posts_channel_id` (`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `schema_migrations`
--

CREATE TABLE IF NOT EXISTS `schema_migrations` (
  `version` varchar(255) DEFAULT NULL,
  UNIQUE KEY `idx_schema_migrations_version` (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `schema_migrations`
--

INSERT INTO `schema_migrations` (`version`) VALUES
('20140402203128'),
('20141115181109'),
('20141116082914'),
('20141211140257');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `fullname`, `email`, `salt`, `password`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Franck Hilcenko', 'franck_hilcenko@hotmail.com', '4820164e62518c.b640bef2', '$2a$13$4820164e62518c.b640beeVAmYbPHHWuSQYallWY4LM.MTpGJw64K', 1, '2015-01-23 10:47:31', '2015-01-23 10:47:31'),
(2, 'blublu', 'knk@qsok', '45eb4c8239c700a3366a8.8', '$2a$13$45eb4c8239c700a3366a8.Oh6H3xPyIuWW16GNoFp/fixctHGflNC', 1, '2015-01-23 21:35:51', '2015-01-23 21:35:51'),
(3, 'blabla', 'blabla@blublu', '400c1212509.7712e027523', '$2a$13$400c1212509.7712e0275ugYV0nGXW3ewn6RpPlTmiIvUoSvVpY6C', 1, '2015-01-24 00:59:46', '2015-01-24 00:59:46');
--
-- Base de données :  `photolocate`
--

-- --------------------------------------------------------

--
-- Structure de la table `parties`
--

CREATE TABLE IF NOT EXISTS `parties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(45) NOT NULL,
  `score` int(11) DEFAULT NULL,
  `token` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `parties`
--

INSERT INTO `parties` (`id`, `pseudo`, `score`, `token`) VALUES
(1, 'Jean', 0, '44444444444444');

-- --------------------------------------------------------

--
-- Structure de la table `photo_partie`
--

CREATE TABLE IF NOT EXISTS `photo_partie` (
  `photo_id` int(11) NOT NULL,
  `partie_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `photo_partie`
--

INSERT INTO `photo_partie` (`photo_id`, `partie_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1);

-- --------------------------------------------------------

--
-- Structure de la table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(45) NOT NULL,
  `url` varchar(45) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `photos`
--

INSERT INTO `photos` (`id`, `titre`, `url`, `latitude`, `longitude`) VALUES
(1, 'Villa Majorelle', 'villa-majorelle.jpg', 48.6856, 6.16393),
(2, 'Tour Thiers', 'tourThiers.jpg', 48.6906, 6.17488),
(3, 'Basilique Sainte Epvre', 'saint_epvre.jpg', 48.6959, 6.17989),
(4, 'Porte Sainte Catherine', 'porteSainteCath.jpg', 48.6956, 6.18993),
(5, 'Place Stanislas', 'place-stanislas.jpg', 48.6939, 6.18303),
(6, 'Parc de la Pépinière', 'pepiniere.jpg', 48.6881, 6.17344),
(7, 'Stade Marcel Picot', 'marcelPicot.jpg', 48.6954, 6.21039),
(8, 'Faculté d''architecture', 'facArchi.jpg', 48.696, 6.19282),
(9, 'Excelsior', 'excelsior-nancy.jpg', 48.6905, 6.17591),
(10, 'Cathedrale', 'cathedrale.jpg', 48.6956, 6.18993);
--
-- Base de données :  `test`
--

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
