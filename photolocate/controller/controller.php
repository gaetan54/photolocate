<?php

namespace photolocate\controller;

use photolocate\model\Partie;
use photolocate\model\Photo;
use photolocate\vue\AccueilVue;
use photolocate\vue\EspaceAdminVue;
use photolocate\vue\GameVue;
use photolocate\vue\GeneralVue;
use photolocate\vue\ScoresVue;
use Slim\Slim;
use Twig_Autoloader;
use Twig_Environment;
use Twig_Loader_Filesystem;
use Upload\File;
use Upload\Storage\FileSystem;
use Upload\Validation\Mimetype;
use Upload\Validation\Size;

class controller {

    /**
     * Page d'acceuil
     */
    public function afficheAccueil(){
        $vue = new AccueilVue();
        echo $vue->render();
    }

    /**
     * Affiche le tableau des scores
     */
    public function afficheScores(){
        $vue = new ScoresVue();
        echo $vue->render();
    }

    /**
     * Page d'administation du jeu : ajout de lieu
     */
    public function espaceAdmin(){
        $vue = new EspaceAdminVue();
        echo $vue->render();
    }

    /**
     * Gestion de l'ajout d'un lieu
     * @param $app
     */
    public function ajoutPhotoAdmin($app){
        $titre = $app->request->post('titre-img');
        $titre = filter_var($titre, FILTER_SANITIZE_SPECIAL_CHARS);

        $lat = $app->request->post('lat');
        $latOK = mb_strimwidth($lat, 0, 10);
        $lat = filter_var($lat, FILTER_SANITIZE_NUMBER_FLOAT);
        $long = $app->request->post('long');
        $lngOK = mb_strimwidth($long, 0, 10);
        $long = filter_var($long, FILTER_SANITIZE_NUMBER_FLOAT);

        if( (isset($_FILES['image1'])) AND ($_FILES['image1']['error'] == 0 ) and (isset($titre)) and (isset($lat)) and (isset($long))) {

            $photo = new Photo();
            $photo->titre = $titre;
            $nomImage = $this->ValidationImage('image1');
            $photo->url = $nomImage;
            $photo->latitude = $latOK;
            $photo->longitude = $lngOK;
            $photo->save();
            $error = 1;

            $vue = new EspaceAdminVue($error);
            echo $vue->render();
        }
        else {
            $error = 2;
            $vue = new EspaceAdminVue($error);
            echo $vue->render();
        }
    }

    /**
     * Fonction de validation et sauvegarde d'image du lieu
     * @param $key
     * @return string
     */
    private function ValidationImage($key){
        $racine = $_SERVER['DOCUMENT_ROOT'];
        $storage = new FileSystem("$racine" . "/photolocate/photolocate/assets/img/");
        $file = new File($key, $storage);
        $new_filename = uniqid();
        $file->setName($new_filename);

        $file->addValidations(array(
            new Mimetype(array('image/png', 'image/jpeg')),
            new Size('10M')
        ));
        $nomImage = $file->getNameWithExtension();
        $file->upload();
        return $nomImage;
    }

    /**
     * API : Creation d'une partie
    */
    public function creationPartie(){
        // recup du pseudo
        $pseudo = filter_var($_POST['pseudo'], FILTER_SANITIZE_SPECIAL_CHARS);

        // generation du token
        $token = uniqid();

        // creation de la partie
        $partie = new Partie();
        $partie->pseudo = $pseudo;
        $partie->token = $token;
        $partie->save();
        $LastInsertId = $partie->id;


        // creation de la response JSON
        $tabPartie = array('partie' => array(
            'pseudo' => $pseudo,
            'token' => $token,
            'id' => $LastInsertId
        ),
        'status' => 'ok');

        $jsonPartie = json_encode($tabPartie, JSON_PRETTY_PRINT);
        $app = Slim::getInstance();
        $app->response->setStatus(200);
        $app->response->headers->set('Content-type', 'application/json');
        $app->response->setBody($jsonPartie);
    }

    /**
     * API : Mise a jour de la partie (push du score final du joueur)
     * @param $id
     * @param $app
     */
    public function majPartie($id, $app){
        $partie = Partie::find($id);
        if ($partie == null){
            $app->response->setStatus(404);
        }
        else{
            $app->response->setStatus(200) ;
            $app->response->headers->set('Content-type','application/json') ;
            $dataRaw = $app->request->getBody();
            $data = json_decode($dataRaw, true);

            $score = $data['score'];
            $partie->score = $score;
            $partie->save();
            $reponse = array('status' => 'OK');
            $reponsePartie = json_encode($reponse, JSON_PRETTY_PRINT);
            $app->response->setBody($reponsePartie);
        }
    }

    /**
     * API : retourne la liste des 10 photos à placer pour dérouler la partie,
     * avec la position exacte de chaque photo sur la carte.
     * @param $id
     * @param $app
     */
    public function recupPhotos($id, $app){
        $tokenParam = $app->request->get('token');
        $partie = Partie::find($id);
        $tabPhotos = Photo::all()->random(10);

        if ($partie == null){
            $tab['status'] = "ERROR_ID";
            echo json_encode($tab, JSON_PRETTY_PRINT);
        }
        else{
            if($tokenParam == $partie['token']){
                $app->response->setStatus(200);
                $app->response->headers->set('Content-type','application/json');
                foreach($tabPhotos as $p){
                    $photo['photo']['id'] = $p['id'];
                    $photo['photo']['titre'] = $p['titre'];
                    $photo['photo']['url'] = $p['url'];
                    $photo['coordonnees'] = array('latitude' => $p['latitude'], 'longitude' => $p['longitude']);
                    $listePhotos['photos'][] = $photo;
                }
                $listePhotos['status'] = 'OK';
                echo json_encode($listePhotos, JSON_PRETTY_PRINT);
            }
            else{
                $tab['status'] = "ERROR_TOKEN";
                echo json_encode($tab, JSON_PRETTY_PRINT);
            }
        }
    }

    /**
     * Api: retourne les meilleurs scores, si il n'y a pas de paramètre ?size, retourne les scores de toutes les parties.
     * @param $app
     */
    public function apiScore($app){
        $app->response->setStatus(200) ;
        $app->response->headers->set('Content-type','application/json');
        $tabPartis = Partie::All()->toArray();
        $tailleTab = count($tabPartis);//récupération du nombre de partie
        $tailleParam = $app->request->get('size');

        if ($tabPartis == null) {//test si objet existe
            $tab['status'] = "ERROR_BD";
            echo json_encode($tab, JSON_PRETTY_PRINT);
            exit();
        }
        if ($tailleTab < $tailleParam){
            $tab['status'] = "ERROR_SIZE";
            echo json_encode($tab, JSON_PRETTY_PRINT);
            exit();
        }
        usort($tabPartis, function($a, $b){// usort() : triage de tableau
            if ( $a['score'] == $b['score'] ) return 0;
            return ($a['score'] < $b['score']) ? 1 : -1; // if ternaire.
        });

        if ($tailleParam == null)//si size null, on renvoi toutes les parties
        {
            $listeScores=  array('scores' => array());
            for($i = 0; $i < $tailleTab; $i++){//boucle: contruction du json
                $scores[$i]['place'] = $i+1;
                $scores[$i]['pseudo'] = $tabPartis[$i]['pseudo'];
                $scores[$i]['id'] = $tabPartis[$i]['id'];
                $scores[$i]['score'] = $tabPartis[$i]['score'];
            }
        }else{//sinon, on renvoi uniquement le nombre de partie définit
            $listeScores=  array('scores' => array());
            for($i = 0; $i < $tailleParam; $i++){//boucle: contruction du json
                $scores[$i]['place'] = $i+1;
                $scores[$i]['pseudo'] = $tabPartis[$i]['pseudo'];
                $scores[$i]['score'] = $tabPartis[$i]['score'];
            }
        }
        $listeScores['scores'] = $scores;
        $listeScores['status'] = 'OK';
        echo json_encode($listeScores, JSON_PRETTY_PRINT);
    }

    /**
     * Api: Affichage description d'une partie (pseudo + score)
     * @param $id
     * @param $app
     */
    public function apiDescription($id, $app){
        $app->response->setStatus(200) ;
        $app->response->headers->set('Content-type','application/json');
        $description = Partie::find($id);
        $tokenParam = $app->request->get('token');

        if ($description == null) {//test si objet existe
            $tab['status'] = "ERROR_ID";
            echo json_encode($tab, JSON_PRETTY_PRINT);
        } else {
            if($tokenParam == $description['token']) {//test du paramètre token. Le param token doit être égal au token de la db
                $tabDescription = array('parties' => array(
                    'pseudo' => $description['pseudo'],
                    'score' => $description['score']
                ));
                $tabDescription['status'] = 'OK';
                echo json_encode($tabDescription, JSON_PRETTY_PRINT);
            } else {//sinon erreur
                $tab['status'] = "ERROR_TOKEN";
                echo json_encode($tab, JSON_PRETTY_PRINT);
            }
        }
    }
}