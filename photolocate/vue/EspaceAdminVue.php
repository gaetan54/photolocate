<?php

namespace photolocate\vue;

use Slim\Slim;

class EspaceAdminVue extends GeneralVue{

    public function __construct($error = null){

        parent::__construct();

        $this->layout = 'formAdmin.html.twig';

        switch ($error) {
            case (1):
                $this->arrayVar['add'] = $error;
                break;
            case (2):
                $this->arrayVar['incomplet'] = $error;
                break;
        }
        $this->arrayVar['urlFormAdmin'] = Slim::getInstance()->urlFor('espaceAdmin');
    }
}