<?php

namespace photolocate\vue;

class GeneralVue extends Vue {

    public function __construct($o = null){

        parent::__construct($o);

        $this->layout = 'General.html.twig';
        $this->arrayVar['o'] = $o;
    }
}