<?php

namespace photolocate\vue;

class ScoresVue extends GeneralVue {

    /**
     * @param null $o
     */
    public function __construct($o = null){

        parent::__construct($o);

        $this->layout = 'Scores.html.twig';
        $this->arrayVar['o'] = $o;
    }
}