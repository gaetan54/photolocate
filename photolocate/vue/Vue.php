<?php

namespace photolocate\vue;

abstract class Vue {

    protected  $layout = null;
    protected  $obj;
    protected  $arrayVar;

    /**
     * Constructeur
     * Initialise la variable $obj
     * @param null $o
     */
    public function __construct($o = null){
        $this->obj = $o;
    }

    /**
     * @param $var
     * @param $val
     */
    public function addVar($var, $val){
        $this->arrayVar[$var] = $val;
    }

    /**
     * Retourne le template sous format le 'string'
     * @return string
     */
    public  function render(){
        $loader = new \Twig_Loader_Filesystem('photolocate/assets/templates');
        $twig = new \Twig_Environment($loader);
        $tmpl = $twig->loadTemplate($this->layout);
        return $tmpl->render($this->arrayVar);
    }

    /**
     * Affiche le template sur le navigateur
     */
    public function display(){
        echo $this->render();
    }
}