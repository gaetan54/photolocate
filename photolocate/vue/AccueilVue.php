<?php

namespace photolocate\vue;

class AccueilVue extends GeneralVue {

    /**
     * @param null $o
     */
    public function __construct($o = null){

        parent::__construct($o);

        $this->layout = 'Accueil.html.twig';
        $this->arrayVar['o'] = $o;
    }
}