gameApp = angular.module('gameApp', ['ngSanitize', 'ngRoute', 'ngResource'])

gameApp.config ['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) ->
  $routeProvider.when '/', {templateUrl: 'photolocate/assets/templates/tplAngular/index.tpl.html', controller: 'IndexCtrl'}
  $routeProvider.when '/play', {templateUrl: 'photolocate/assets/templates/tplAngular/play.tpl.html', controller: 'PlayCtrl'}
  $routeProvider.when '/terminer', {templateUrl: 'photolocate/assets/templates/tplAngular/terminer.tpl.html', controller: 'TerminerCtrl'}
  $routeProvider.otherwise('/')
]
