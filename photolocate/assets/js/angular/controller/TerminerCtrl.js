angular.module('gameApp').controller('TerminerCtrl', ['$scope', '$http', '$location', '$rootScope', function ($scope, $http, $location, $rootScope) {
    $scope.init2 = function() {
        $scope.total = $rootScope.pointsTotal;
        $http({
            method: 'PUT',
            url: 'play/games/' + $rootScope.infoPartie.partie.id + '?token=' + $rootScope.infoPartie.partie.token,
            data: {
                score: $rootScope.pointsTotal
            }
        })
            .success(function (data) {
                var LeafIcon = L.Icon.Default.extend({
                    options: {
                        iconUrl: 'photolocate/assets/img/imgSite/marker-icon-green.png'
                    }
                });
                var greenIcon = new LeafIcon;

                $scope.info = data;
                var map = L.map('map3').setView([48.6856292, 6.1547173], 14);
                L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                }).addTo(map);

                /*for(i = 0; i < 10; i++) {
                    if($rootScope.tabLatPlayer[i] != null && $rootScope.tabLngPlayer[i] != null)
                    L.marker([$rootScope.tabLatPlayer[i], $rootScope.tabLngPlayer[i]]).addTo(map)
                }*/

                for(j = 0; j < 10; j++) {
                    if($rootScope.tabLatImg[j] != null && $rootScope.tabLngImg[j] != null)
                        L.marker([$rootScope.tabLatImg[j], $rootScope.tabLngImg[j]], {icon: greenIcon}).addTo(map).bindPopup('<img class="imgPopup" src="photolocate/assets/img/'+$rootScope.tabImageTotal[j]+'" ><br />'+$rootScope.tabNomImage[j])
                }
            })
            .error(function (data, status, headers, config) {
                // Une erreur est survenue
            });
    }
}]);

angular.module('gameApp').controller('ClassementCtrl', ['$scope', '$http', '$location', '$rootScope', function ($scope, $http, $location, $rootScope) {
    $scope.init3 = function () {
        $http({
        method: 'GET',
        url: 'play/scores'
        })
        .success(function (data) {
            var taille = 0;
            for(var i in data.scores) {
                if($rootScope.infoPartie.partie.id == data.scores[i].id)
                    $scope.classementJoueur = data.scores[i].place;

                taille++;
            }
            $scope.taille = taille;
        })
    }
}]);

