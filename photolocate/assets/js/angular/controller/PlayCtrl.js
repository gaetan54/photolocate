angular.module('gameApp').controller('PlayCtrl', ['$scope', '$http', '$location', '$rootScope', function ($scope, $http, $location, $rootScope) {
    $scope.ptsTot = 0;
    /*var tabLatPlayer = [];
    var tabLngPlayer = [];
    var j = 0;
    var k = 0;*/
    var increment = 0;

    var tabImageTotal = [];
    var tabNomImage = [];

    var tabLatImg = [];
    var tabLngImg = [];
    var l = 0;
    var m = 0;
    $scope.listner =  function (e) {
        var response = null;
        var points = 0;
        var time = 0;
        var LeafIcon = L.Icon.Default.extend({
            options: {
                iconUrl: 'photolocate/assets/img/imgSite/marker-icon-green.png'
            }
        });
        var greenIcon = new LeafIcon;

        chronoStop();
        $("#chrono").hide();
        $('.results').show();

        $('#resultTemps').text(finalsec + '.' + msec);

        $('.btnSuivant').show();

        if (response != null)
            this.removeLayer(response);

        response = L.marker([e.latlng.lat, e.latlng.lng]).addTo(this);
        position = L.marker([$scope.photo.coordonnees.latitude, $scope.photo.coordonnees.longitude], {icon: greenIcon}).addTo(this);
        distance = response.getLatLng().distanceTo(position.getLatLng()).toFixed(2);
        response.bindPopup('Vous êtes à ' + distance + ' m de l\'objectif').openPopup();

        if (sec < 5) {
            time = 4;
        } else if (sec < 10) {
            time = 2;
        } else if (sec >= 15) {
            time = 0;
        }
        if (distance < 100) {
            points = 4 * time;
        } else if (distance < 200) {
            points = 2 * time;
        } else if (distance >= 300) {
            points = 1;
        }
        if ($scope.ptsTot == 0) {
            $('#resultPoints').text(points);
        }
        $scope.ptsTot = $scope.ptsTot + points;
        $('#resultPoints').text($scope.ptsTot);

        $rootScope.pointsTotal = $scope.ptsTot;

        this.off('click');
        $scope.laPosition = position;
        $scope.laResponse = response;

        /*tabLatPlayer[j] = e.latlng.lat;
        j = j+1;

        tabLngPlayer[k] = e.latlng.lng;
        k = k + 1;*/

        tabLatImg[l] = $scope.photo.coordonnees.latitude;
        l = l+1;

        tabLngImg[m] = $scope.photo.coordonnees.longitude;
        m = m+1;

        tabImageTotal[increment] = $scope.photo.photo.url;

        tabNomImage[increment] = $scope.photo.photo.titre;

        increment = increment + 1;
    };

    $scope.nextImage = function () {
        $scope.i++;
        $scope.indiceImg++;
        if($scope.i > 9 ) {
            $location.path('/terminer')
        }
        $scope.photo = $scope.info['photos'][$scope.i];
        $scope.laMap.removeLayer($scope.laPosition);
        $scope.laMap.removeLayer($scope.laResponse);
        $scope.laMap.on('click', $scope.listner);
        chronoStart();

        $('#resultTemps').text('0');
        $('.btnSuivant').hide();
        $('#chrono').show();

        $('.results').hide();
        chronoStart();

        /*$rootScope.tabLatPlayer = tabLatPlayer;
        $rootScope.tabLngPlayer = tabLngPlayer;*/

        $rootScope.tabLatImg = tabLatImg;
        $rootScope.tabLngImg = tabLngImg;
    };

    $scope.init = function() {
        $http({
            method: 'GET',
            url: 'play/games/' + $rootScope.infoPartie.partie.id + '/photos?token=' + $rootScope.infoPartie.partie.token
        })
            .success(function (data) {
                $scope.info = data;
                $scope.photo = $scope.info['photos'][0];
                $scope.bouton = true;

                // create a map in the "map" div, set the view to a given place and zoom
                var map = L.map('map2').setView([48.69084441778115, 6.1825574934482574], 14);

                // add an OpenStreetMap tile layer
                L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                }).addTo(map);

                map.on('click', $scope.listner);
                $scope.laMap = map;
            })
            .error(function (data, status, headers, config) {
                // Une erreur est survenue
            });
        $scope.i = 0;
        $scope.indiceImg = 1;
        $rootScope.tabImageTotal = tabImageTotal;
        $rootScope.tabNomImage = tabNomImage;
    }
}]);