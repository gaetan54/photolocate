angular.module('gameApp').controller('IndexCtrl', ['$scope', '$http', '$location', '$rootScope', ($scope, $http, $location, $rootScope) ->
  $scope.creaPlayer = ->
    $http
      method: 'POST',
      url: 'play/games',
      headers:
        'Content-Type': 'application/x-www-form-urlencoded',
      transformRequest: (obj)->
        str = []
        for p of obj
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]))
          str.join("&")
      data: {pseudo: $scope.pseudo}
    .success (data) ->
      $scope.info = data
      $rootScope.infoPartie = $scope.info
      $location.path('/play')
    .error (data) ->

])
angular.module('gameApp').controller('IndexScore', ['$scope', '$http', '$location', ($scope, $http, $location) ->
  $http
    method: 'GET',
    url: 'play/scores'
  .success (data) ->
    $scope.scores = data.scores
  .error (data) ->
])