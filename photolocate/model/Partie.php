<?php

namespace photolocate\model;

use Illuminate\Database\Eloquent\Model;

class Partie extends Model{

    public $timestamps = false;

    public function photos(){
        return $this->belongsToMany('model\Photo');
    }
}