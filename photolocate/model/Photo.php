<?php

namespace photolocate\model;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model{

    public $timestamps = false;

    public function parties(){
        return $this->belongsToMany('model\Partie');
    }
}