<?php
/**
 * Created by PhpStorm.
 * User: iman
 * Date: 28/01/15
 * Time: 16:17
 */

namespace Slim\Middleware;


class CsrfGuard extends \Slim\Middleware {

    /**
     * call
     */
    public function call()
    {
            $this->next->call(); // à placer en premier pour lancer l'application et du coup récup la response complétée
            $this->csrfguard_replace_forms();
            $this->csrfguard_start();

    }

    /**
     * @param $key
     * @param $value
     */
    protected function store_in_session($key,$value)
    {
        if (isset($_SESSION))
        {
            $_SESSION[$key]=$value;
        }
    }

    protected function unset_session($key)
    {
        $_SESSION[$key]=' ';
        unset($_SESSION[$key]);
    }

    /**
     * @param $key
     * @return bool
     */
    protected function get_from_session($key)
    {
        if (isset($_SESSION[$key]))
        {
            return $_SESSION[$key];
        }
        else {  return false; }
    }

    /**
     * @param $unique_form_name
     * @return string
     */
    protected function csrfguard_generate_token($unique_form_name)
    {
        if (function_exists("hash_algos") and in_array("sha512",hash_algos()))
        {
            $token=hash("sha512",mt_rand(0,mt_getrandmax()));
        }
        else
        {
            $token=' ';
            for ($i=0;$i<128;++$i)
            {
                $r=mt_rand(0,35);
                if ($r<26)
                {
                    $c=chr(ord('a')+$r);
                }
                else
                {
                    $c=chr(ord('0')+$r-26);
                }
                $token.=$c;
            }
        }
        $this->store_in_session($unique_form_name,$token);
        return $token;
    }

    /**
     * @param $unique_form_name
     * @param $token_value
     * @return bool
     */
    protected function csrfguard_validate_token($unique_form_name,$token_value)
    {
        $token= $this->get_from_session($unique_form_name);
        if ($token===false)
        {
            return false;
        }
        elseif ($token===$token_value)
        {
            $result=true;
        }
        else
        {
            $result=false;
        }
        $this->unset_session($unique_form_name);
        return $result;
    }

    /**
     * Permet de modifier les formulaires dans la response en ajoutant les input pour csrf
     *
     * @return mixed|string
     */
    protected function csrfguard_replace_forms()
    {
        $app = $this->app;
        $res = $app->response;
        $body = $res->getBody();
        $count=preg_match_all("/<form(.*?)>(.*?)<\\/form>/is",$body,$matches,PREG_SET_ORDER);
        if (is_array($matches))
        {
            foreach ($matches as $m)
            {
                if (strpos($m[1],"nocsrf")!==false) { continue; }
                $name="CSRFGuard_".mt_rand(0,mt_getrandmax());
                $token=$this->csrfguard_generate_token($name);
                $body=str_replace($m[0],
                    "<form{$m[1]}><input type='hidden' name='CSRFName' value='{$name}' /><input type='hidden' name='CSRFToken' value='{$token}' />{$m[2]}</form>", $body);
            }
            $res->setBody($body);
        }
        return $body;
    }

    /**
     * vérifie la présence et la validité du CSRFToken et du CSRFName
     */
    protected function csrfguard_start()
    {
        if (count($_POST) && isset($_POST['validFormAdmin']))
        {
            if ( !isset($_POST['CSRFName']) or !isset($_POST['CSRFToken']) )
            {
                trigger_error("No CSRFName found, probable invalid request.",E_USER_ERROR);
            }
            $name =$_POST['CSRFName'];
            $token=$_POST['CSRFToken'];
            if (!$this->csrfguard_validate_token($name, $token))
            {
                trigger_error("Invalid CSRF token.",E_USER_ERROR);
            }
        }
    }
}